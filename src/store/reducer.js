import actionType from './actionType';

const initialState = {

    value: '',
    message: '',
    isTurn: false

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.ADD_ELEM:
            if (state.value.length < 4) {
                return {...state, value: state.value + action.text};
            }
            return state;

        case actionType.CLEAR:
            return {...state, value: state.value.slice(0, -1)};

        case actionType.FIND:
            if (state.value === '2905') {
                let message = 'Access Granted';
                return {...state, message: message, isTurn: true}
            }
            return state;
        default:
            return state;
    }
};

export default reducer;