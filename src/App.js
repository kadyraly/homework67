import React, { Component } from 'react';

import './App.css';
import PinCode from "./container/PinCode/PinCode";

class App extends Component {
  render() {
    return (
      <PinCode />
    );
  }
}

export default App;
