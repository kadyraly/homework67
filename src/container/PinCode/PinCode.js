import React, {Component} from  'react';
import './PinCode.css';

import {connect} from 'react-redux';

class Counter extends Component {

    btns = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    render() {
        return (
            <div className="PinCode">
                <div className="Input" style={{background: this.props.isTurn ? 'green' : null}}>
                    {new Array(this.props.value.length).fill('*').join('')}
                </div>
                <div className="Button">
                    {this.btns.map((btn) => {
                        return <button onClick={() => this.props.addElem(btn)}>{btn}</button>
                    })}

                    <button onClick={() => this.props.clear()}>-</button>
                    <button onClick={() => this.props.find()}>E</button>
                    <div>{this.props.message}</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        value: state.value,
        message: state.message,
        isTurn: state.isTurn
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        addElem: (text) => {
            dispatch({
                type: 'ADD_ELEM',
                text: text
            })
        },
        clear: (text) => {
            dispatch({
                type: 'CLEAR',
                text: text
            })
        },
        find: () => {
            dispatch({
                type: 'FIND',

            })
        }

    }
};
export default connect(mapStateToProps, mapDispatchToProps )(Counter);
